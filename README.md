Texture carre ;
    if (!carre.loadFromFile("greensquare.png"))
        printf("PB de chargement de l'image %s !\n", "greensquare.png");

    Sprite tableausprite[400];
    int j;
    for(j=0; j<10; j++)
    {
        tableausprite[j].setTexture(carre);
        tableausprite[j].setPosition(rand()%500, rand()%500);
        window.draw(tableausprite[j]);
    }
    window.display();